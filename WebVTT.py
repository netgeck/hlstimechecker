from datetime import datetime
from typing import NamedTuple


class SubtItem(NamedTuple):
    id: int
    value: str
    # first_segment: int
    # end_segment: int
    time_start_s: int
    time_stop_s: int
    # finish: bool


class WebVTTParser:
    subtitles = []

    def __init__(self):
        self.subtitles.clear()

    def read_subtitles(self, file):
        file_vtt = open(file, 'r', encoding='utf-8')
        Lines = file_vtt.readlines()

        count = 0
        opened = False

        entry_id = 0
        entry_value = ""
        entry_time_start = 0
        entry_time_stop = 0
        for line in Lines:
            if line == "\n":
                if opened:
                    self.subtitles.append(SubtItem._make([entry_id, entry_value.rstrip(), entry_time_start, entry_time_stop]))
                    entry_id += 1
                    entry_value = ""
                    entry_time_start = 0
                    entry_time_stop = 0
                    opened = False
                continue
            if "-->" in line:
                entry_time_start, entry_time_stop = self.get_entry_timestamp(line)
                # print(f"open: {line.rstrip()}  start {entry_time_start}  stop {entry_time_stop}")
                opened = True
                continue
            if opened:
                entry_value += line

    def get_entry_timestamp(self, timestamp_line):
        date_str_begin = timestamp_line.split("-->")[0]
        date_str_end = timestamp_line.split("-->")[1]
        # entry_time_start = datetime.strptime('1970 ' + date_str_begin.strip(), '%Y %H:%M:%S.%f')
        base = datetime(1900, 1, 1)
        time_start = (datetime.strptime(date_str_begin.strip(), '%H:%M:%S.%f') - base).total_seconds()
        time_stop = (datetime.strptime(date_str_end.strip(), '%H:%M:%S.%f') - base).total_seconds()
        return time_start, time_stop