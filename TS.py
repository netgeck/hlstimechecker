import struct
from collections import namedtuple
from enum import Enum


class TSconstant(Enum):
    SyncByte = 0x47
    table_id_PAT = 0x00     # Program Association Table
    table_id_PMT = 0x02     # Program Map Table


def seek_to_byte(f, byte):
    """
    Seek file to byte
    """
    while True:
        b = f.read(1)
        if not b:   # EOF
            return False

        ret = struct.unpack("B", b)[0]    # unsigned char
        if ret == byte:
            return True


def timestamp_read(f):
    """
    Unpack DTS / PTS
    """
    msb = struct.unpack("B", f.read(1))[0]
    lsb = struct.unpack("!I", f.read(4))[0]
    timestamp = (lsb & 0xffff) >> 1
    timestamp = timestamp | ((lsb >> 17) << 15)
    timestamp = timestamp | (((msb & 0xE) >> 1) << 30)
    return timestamp


class TSHeader:
    def __init__(self, header_big_endian_4bytes):
        self.header = struct.unpack(">I", header_big_endian_4bytes)[0]  # big-endian unsigned int

    def pid(self):
        pid = (self.header & 0x7ff00) >> 8
        return pid

    def payload_unit_start_indicator(self):
        payload_unit_start_indicator = (self.header & 0x400000) >> 22
        return payload_unit_start_indicator

    def adaptation_field_control(self):
        adaptation_field_control = (self.header & 0x30) >> 4
        return adaptation_field_control


Frame = namedtuple('Frame', ['num', 'pts', 'dts', 'dur', 'warn_store_dts_same_pts'])


class HlsSegment:
    """
    Naive parsing of limited format TS segments
    All work does in Constructor
    """
    next_packet = 0
    frames = []

    def __init__(self, file_name):
        self.next_packet = 0
        self.frames.clear()
        stream_type, elementary_PID = self.get_media_stream_pid(file_name)
        if stream_type is None or elementary_PID is None:
            print("Not found Stream in PMT")
            exit(0)
        print("Found stream_type ", hex(stream_type), " PID ", elementary_PID)

        self.iterate_PES_packets(file_name, elementary_PID)

    def get_media_stream_pid(self, file_name):
        with open(file_name, "rb") as f:
            if not seek_to_byte(f, TSconstant.SyncByte.value):
                print("PAT: No synchro byte in file")
                return (None, None)
            f.seek(-1, 1)  # seek to the position before SyncByte

            pat_header = self.seek_to_packet(f, 0x0)    # seek to PAT table
            if pat_header is None:
                print("PAT: No PAT table found")
                return (None, None)

            if pat_header.payload_unit_start_indicator():
                f.seek(1, 1)

            program_number, program_PID = self.parse_PAT_section(f)
            if program_number is None or program_PID is None:
                print("PAT: Not found Program")
                return (None, None)

            print("PAT: Found Program ", program_number, " PID ", program_PID)
            pmt_header = self.seek_to_packet(f, program_PID)
            if pmt_header is None:
                print("PMT: Not found PMT")
                return (None, None)

            if pmt_header.payload_unit_start_indicator():
                f.seek(1, 1)

            return self.parse_PMT_section(f)

    def seek_to_packet(self, f, pid):
        while True:
            f.seek(self.next_packet * 188)
            self.next_packet += 1

            unsigned_int = f.read(4)
            if not unsigned_int:    # EOF
                return None
            header = TSHeader(unsigned_int)
            # print("Packet header ", hex(header), " PID: ", found_pid)
            if header.pid() == pid:
                return header

    def parse_PAT_section(self, f):
        """
        :param f:   file-object pointed to payload containing PAT section
        :return:    program number, program PID
        """
        table_id = struct.unpack("B", f.read(1))[0]     # unsigned char
        if table_id != TSconstant.table_id_PAT.value:
            print("PAT: Wrong table_id: ", hex(table_id))
            return (None, None)

        bb = struct.unpack(">H", f.read(2))[0]  # big-endian unsigned short
        section_length = bb & 0xfff     # 12 bit
        # print("PAT section_length ", section_length)
        pat_remaining_header_size = 5
        crc_size = 4
        program_list_length = section_length - pat_remaining_header_size - crc_size
        if program_list_length > 4:
            print("PAT: Warning! Only single Program TS accessible! Programs number ", program_list_length / 4)

        f.seek(pat_remaining_header_size, 1)   # rewind remaining header to the list of programs

        program_number = struct.unpack(">H", f.read(2))[0]
        bb = struct.unpack(">H", f.read(2))[0]
        program_PID = bb & 0x1FFF
        return program_number, program_PID

    def parse_PMT_section(self, f):
        table_id = struct.unpack("B", f.read(1))[0]     # unsigned char
        if table_id != TSconstant.table_id_PMT.value:
            print("PMT: Wrong table_id: ", hex(table_id))
            return (None, None)

        bb = struct.unpack(">H", f.read(2))[0]  # big-endian unsigned short
        section_length = bb & 0xfff     # 12 bit
        # print("PMT section_length ", section_length, "  (raw", hex(bb), ")")
        remaining_header_length = 9
        crc_size = 4
        program_list_length = section_length - remaining_header_length - crc_size

        f.seek(remaining_header_length - 2, 1)
        bb = struct.unpack(">H", f.read(2))[0]  # big-endian unsigned short
        program_info_length = bb & 0xfff  # 12 bit
        if program_info_length > 0:
            print("PMT: PMT descriptors is not accessible.  program_info_length ", program_info_length)
            return (None, None)

        if program_list_length > 5:
            print("PMT: Warning! Only single program stream accessible! Streams in PMT ", program_list_length / 5)

        stream_type = struct.unpack("B", f.read(1))[0]      # unsigned char

        bb = struct.unpack(">H", f.read(2))[0]  # big-endian unsigned short
        elementary_PID = bb & 0x7ff
        return stream_type, elementary_PID

    def iterate_PES_packets(self, file_name, PID):
        """
        Parse PTS, DTS while while iterating through file by PES packets of given PID
        """
        self.next_packet = 0
        skipped_packets = 0
        parsed_cnt = 0
        with open(file_name, "rb") as f:
            while True:
                header = self.seek_to_packet(f, PID)
                if header is None:
                    # print("PES: No more packets. Parsed ", parsed)
                    return

                if header.payload_unit_start_indicator():    # payload has PES header
                    packet_pos = f.tell() - 4
                    # print("PES: Packet N", self.next_packet - 1 ," (skipped ", skipped_packets, ")  file pos ", packet_pos, "(", hex(packet_pos), ")")

                    skipped_packets = 0

                    if header.adaptation_field_control() != 0b01:   # if not 'No adaptation_field, payload only'
                        if (header.adaptation_field_control() == 0b10):     # Adaptation_field only, no payload
                            continue
                        if (header.adaptation_field_control() == 0b11):     # Adaptation_field followed by payload
                            adaptation_field_length = struct.unpack("B", f.read(1))[0]    # unsigned char
                            f.seek(adaptation_field_length, 1)  # skip Adaptation field

                    res, pts, dts, warn_store_dts_same_pts = self.parse_PES_section(f)
                    if not res:
                        exit(0)

                    if pts is not None:
                        # print("PTS ", pts, "  DTS ", dts)
                        self.frames.append(Frame._make([parsed_cnt, pts, dts, 0, warn_store_dts_same_pts]))

                    parsed_cnt += 1

                else:
                    skipped_packets += 1

    def parse_PES_section(self, f):
        """
        Parse PES
        :param f:
        :return: bool error, pts, dts       if pts is not None, dts is not None too
        """
        bbbb = struct.unpack(">I", f.read(4))[0]    # big-endian unsigned int
        packet_start_code_prefix = bbbb >> 8  # first 3 bytes
        if packet_start_code_prefix != 0x000001:
            print("PES: packet_start_code_prefix: ", hex(packet_start_code_prefix))
            return False, None, None, None

        stream_id = bbbb & 0xff
        if stream_id != 0xE0 and stream_id != 0xC0 and stream_id != 0xBD:
            #           ^video                ^audio                ^private_stream_1
            print("PES: unknown stream_id ", hex(stream_id))

        f.seek(2, 1)    #PES_packet_length = struct.unpack(">H", f.read(2))[0]   # big-endian uint16_t

        b = struct.unpack("B", f.read(1))[0]    # unsigned char
        if ((b & 0xC0) >> 6) != 0b10:   # 2 bits always '10'
            print("PES: Corrupted PES header")
            return False, None, None, None

        b = struct.unpack("B", f.read(1))[0]  # unsigned char
        PTS_DTS_flags = (b & 0xC0) >> 6     # 00 - no PTS/DTS,  10 - only PTS,  11 - both,  01 - forbidden

        f.seek(1, 1)    # remaining header

        pts = None
        dts = None
        store_same_pts = False
        if PTS_DTS_flags == 2:
            pts = timestamp_read(f)
            dts = pts
        if PTS_DTS_flags == 3:
            pts = timestamp_read(f)
            dts = timestamp_read(f)
            if pts == dts:
                store_same_pts = True

        return True, pts, dts, store_same_pts