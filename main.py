#!/usr/bin/python

import os
import statistics
import sys
import argparse

from natsort import natsorted

from TS import HlsSegment
from WebVTT import WebVTTParser

parser = argparse.ArgumentParser(description='Check timestamps in .ts segments')
parser.add_argument("--dry", help="only show list of files to process", action="store_true")
parser.add_argument('path', help='directory or single segment for analysis')
parser.add_argument("--media-playlist", help="Media playlist for given segments")
parser.add_argument("--subtitles", help="switch to subtitles analyze mode", action="store_true")
parser.add_argument("--verbose", action="store_true")
parser.add_argument('-N', '--num', help="Number of segments to analyze", type=int, default=0)

try:
    options = parser.parse_args()
except:
    parser.print_help()
    sys.exit(0)


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def fill_durations(frames):
    frames_display_order = sorted(frames, key=lambda tup: tup.pts)

    for i in range(len(frames_display_order)):
        frame = frames_display_order[i]
        frame_next = None
        if i + 1 < len(frames_display_order):
            frame_next = frames_display_order[i + 1]
        if frame_next:
            duration = frame_next.pts - frame.pts
            frames[frame.num] = frames[frame.num]._replace(dur=duration)


def duration_to_sec(dur):
    return dur / 90000


def get_info_from_playlist(playlist, segment):
    meta = None
    line_segment = ''
    for line in playlist:
        if line.startswith("#EXT"):
            meta = line
        else:   # segment URI
            if '/' in line:
                line_segment = line.split("/")[-1]
            else:
                line_segment = line
            # print(line_segment)
            if segment in line_segment:
                return True, meta
    return False, None


def get_duration_from_playlist(playlist, segment_name):
    with open(playlist, "r") as pl:
        segment_found, info = get_info_from_playlist(pl, segment_name)
        if segment_found:
            tag_value = info.split("#EXTINF:")[1]
            duration_str = tag_value.split(',')[0]
            return float(duration_str)
        return None


def parse_playlist(playlist):
    segments = {}
    with open(playlist, "r") as pl:
        meta = None
        line_segment = ''
        segment_found = False
        segment_duraion = 0
        segment_start_time = 0
        for line in pl:
            if line == "\n":
                continue
            if line.startswith("#"):    # Meta-info
                # print("Some meta: ", line.rstrip())
                if line.startswith("#EXTINF:"):     # segment time info
                    tag_value = line.split("#EXTINF:")[1]
                    duration_str = tag_value.split(',')[0]
                    segment_duraion = float(duration_str)
                    # print("Find segment ", line.rstrip())
                    segment_found = True
                    continue
                else:
                    continue
            elif segment_found:                 # segment URI
                if '/' in line:
                    segment_name = line.split("/")[-1]
                else:
                    segment_name = line
                segments[segment_name.rstrip()] = (segment_start_time, segment_duraion)
                # print(f"Store segment start {segment_start_time} dureation {segment_duraion}\n")
                segment_start_time += segment_duraion
                segment_found = False
        return segments
    return None


def show_analytic(segment_frames, duration_by_playlist):
    stat_playlist_segment_duration = duration_by_playlist

    stat_total_duration = 0
    for frame in segment_frames:
        stat_total_duration += frame.dur
    stat_avg_duration = stat_total_duration / (len(segment_frames) - 1)  # len - 1 : last frame in segment always 0

    durations = []
    for frame in segment_frames:
        durations.append(frame.dur)
    stat_middle_duration_by_PTS = statistics.median(durations)
    stat_total_duration_by_middle = stat_middle_duration_by_PTS * len(segment_frames)

    dts_previous = 0
    stat = ''
    for frame in segment_frames:
        frame_description \
            = "Frame " + str(frame.num) + " PTS=" + str(frame.pts) + " DTS=" + str(frame.dts) + " dur=" + str(frame.dur)

        info = ''
        warnings = ''
        errors = ''
        if frame.num == 0:
            info += f" first frame (PTS={duration_to_sec(frame.pts):.2f} sec)"
        elif frame.num == len(segment_frames) - 1:
            info += " last frame "

        if frame.warn_store_dts_same_pts:
            warnings += " Storing DTS==PTS; "
        if frame.dts > frame.pts:
            warnings += " DTS > PTS; "
        if dts_previous and frame.dts:
            if frame.dts < dts_previous:
                errors += " DTS decrease -" + str(dts_previous - frame.dts) + "; "
            if frame.dts == dts_previous:
                errors += " DTS equal to previous; "
        if frame.dur:
            if frame.dur < stat_middle_duration_by_PTS:
                warnings += " Short duration; "
            elif frame.dur > stat_middle_duration_by_PTS:
                warnings += " Big duration; "

        if errors:
            frame_description += f"  {bcolors.FAIL}" + errors + f"{bcolors.ENDC}"
        if warnings:
            frame_description += f"  {bcolors.WARNING}" + warnings + f"{bcolors.ENDC}"
        if info:
            frame_description += f"  {bcolors.OKBLUE}" + info + f"{bcolors.ENDC}"

        if options.verbose or errors or warnings or info:
            stat += frame_description + "\n"

        dts_previous = frame.dts

    stat += "Segment statistic:\n"
    stat += f"Segment duration (frames * middle_duration): {duration_to_sec(stat_total_duration_by_middle):.2f} sec" \
            + f"\t\tby PTS: {duration_to_sec(stat_total_duration):.3f} sec\n"
    stat += f"Frame duration (middle): {stat_middle_duration_by_PTS} ({duration_to_sec(stat_middle_duration_by_PTS):.5f} sec)" \
            + f"\t\tavg by PTS: {stat_avg_duration} ({duration_to_sec(stat_avg_duration):.5f} sec)\n"
    if stat_playlist_segment_duration:
        stat += f"Segment duration by playlist: {stat_playlist_segment_duration} sec\n"

    return stat


if __name__ == "__main__":
    ordered_files = []

    if os.path.isfile(options.path):
        print("Work with single file: " + options.path)
        ordered_files = [options.path]
    elif os.path.isdir(options.path):
        extension = ".vtt" if options.subtitles else ".ts"
        print(f"Find *{extension} in directory " + options.path)
        for root, dirs, files in os.walk(options.path):
            files = natsorted(files)
            for file in files:
                if os.path.splitext(file)[1] == extension:
                    filePath = os.path.join(root, file)
                    ordered_files.append(filePath)
    else:
        print("Unknown path")
        exit(0)

    if options.dry:
        print(ordered_files)
        sys.exit(0)

    playlist_segments = {}
    if options.media_playlist:
        print("\nPlaylist: ", options.media_playlist)
        #duration_by_playlist = get_duration_from_playlist(options.media_playlist, os.path.basename(file))
        playlist_segments = parse_playlist(options.media_playlist)
        for segment in playlist_segments:
            print(f"segment: {segment} start {playlist_segments[segment][0]} duration {playlist_segments[segment][1]}")
        print("")

    number = 0
    for file in ordered_files:
        number += 1

        duration_by_playlist = 0
        if len(playlist_segments):
            # print(os.path.basename(file))
            segment_base_filename = os.path.basename(file)
            if segment_base_filename not in playlist_segments:
                print(f"No such file in playlist: {segment_base_filename}")
                continue
            duration_by_playlist = playlist_segments[segment_base_filename][1]

        if len(playlist_segments):
            start = playlist_segments[os.path.basename(file)][0]
            stop = playlist_segments[os.path.basename(file)][0] + playlist_segments[os.path.basename(file)][1]
            print(f"Analyze {file}   {start:.3f} - {stop:.3f}")
        else:
            print(f"Analyze {file}")

        if options.subtitles:  # .vtt
            vtt = WebVTTParser()
            vtt.read_subtitles(file)

            # print("Found subtitles:")
            for subt in vtt.subtitles:
                subt_dur = subt.time_stop_s - subt.time_start_s
                print(f"  time {subt.time_start_s:.3f} - {subt.time_stop_s:.3f}  dur {subt_dur:.3f}  str:\"{subt.value}\"")
                print()

        else:                  # .ts
            segment = HlsSegment(file)  # Read segment and parse its timestamps: ['num', 'pts', 'dts', 'dur', 'warn_store_dts_same_pts']

            fill_durations(segment.frames)

            res = show_analytic(segment.frames, duration_by_playlist)
            print(res)

        if options.num:
            print()
            if number >= options.num:
                print("Segments analyze limited by ", options.num, " segments")
                exit(0)
